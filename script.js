const botao = document.querySelector("button");
let cidadeInputada2 = document.querySelector("input");
let msg = document.querySelector("p");

let cidadeInputada;
let woeidBuscado;

function trataRespostaHTTP(resposta) {
    return resposta.json();
}

function chamaAPICidade() {
    cidadeInputada = cidadeInputada2.value;
    fetch(`https://www.metaweather.com/api/location/search/?query=${cidadeInputada}`)
        .then(trataRespostaHTTP)
        .then(trataGetCidade);
}

function chamaAPITempo() {
    fetch(`https://www.metaweather.com/api/location/${woeidBuscado}`)
        .then(trataRespostaHTTP)
        .then(trataGetTempo);
}

function trataGetCidade(dados) {
    woeidBuscado = dados[0].woeid;
    chamaAPITempo();
}

function trataGetTempo(dados) {
    let tempo = dados.consolidated_weather[0].weather_state_name;
    msg.innerHTML = tempo;
}

botao.onclick = chamaAPICidade;